import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'select'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  panelOpenState = false;
  camaras = [
    { nombre: 'CÁMARA DE COMERCIO DE CALI', diligenciado: 'Diligenciado 35/35', firmado: 'Firmado 35/35', pagado: 'Pagado 30/35'},
    { nombre: 'CÁMARA DE COMERCIO DE ORIENTE ANTIOQUEÑO', diligenciado: 'Diligenciado 35/35',
    firmado: 'Firmado 30/35', pagado: 'Pagado 0/35' },
    { nombre: 'CÁMARA DE COMERCIO DE PALMIRA', diligenciado: 'Diligenciado 0/35', firmado: 'Firmado 35/35', pagado: 'Pagado 35/35'},
    { nombre: 'CÁMARA DE COMERCIO DE CALI', diligenciado: 'Diligenciado 35/35', firmado: 'Firmado 35/35', pagado: 'Pagado 30/35'},
    { nombre: 'CÁMARA DE COMERCIO DE ORIENTE ANTIOQUEÑO', diligenciado: 'Diligenciado 35/35',
    firmado: 'Firmado 30/35', pagado: 'Pagado 0/35' },
    { nombre: 'CÁMARA DE COMERCIO DE PALMIRA', diligenciado: 'Diligenciado 0/35', firmado: 'Firmado 35/35', pagado: 'Pagado 35/35'}
  ];
  message = 'selecciono';
  cont = 1;
  ext: string;
  nombrearchivo: string;
  @Output() messageEvent = new EventEmitter<string>();

  constructor() {
    $(() => {
      $('#seleccionar-todos').change(function() {
        $('input[type=checkbox]').prop('checked', $(this).is(':checked'));
      });
    });
  }

  sendMessage() {
    this.messageEvent.emit(this.message);
  }
  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  Descargar() {
    if (this.cont <= 0) {
      console.error('Error, No se puede descargar el archivo');
    } else if (this.cont === 1) {
      this.ext = 'pdf';
      this.nombrearchivo = 'Listado_Formulario_NumeroCamaraComercio_ CUF-AAAAMMDDHHMMSS.' + this.ext;
    } else {
      this.ext = 'zip';
      this.nombrearchivo = 'Listado_Formulario_NumeroCamaraComercio_ CUF-AAAAMMDDHHMMSS.' + this.ext;
    }
  }
  applyFilter(filterValue: string) {

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
}
export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
  {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
  {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
  {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
  {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
  {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
  {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
  {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
  {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
  {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
];
