import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-firma',
  templateUrl: './firma.component.html',
  styleUrls: ['./firma.component.css']
})
export class FirmaComponent implements OnInit {

  firma: false;
  message: string;
  disabled: boolean;
  constructor() {
    // this.disabled = true;
  }

  ngOnInit() {
  }

  receiveMessage($event) {
    this.message = $event;
    // this.disabled = false;
  }
  conf() {

    if (this.message === 'selecciono') {
      Swal.fire({
        title: 'Mensaje de confirmación',
        icon: 'success',
        text: 'El proceso de firma se realizó exitosamente en los establecimientos seleccionados,' +
          'ahora puede realizar el proceso de pago, o seguir firmando otros' +
          'establecimientos asociados al propietario',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText:
          'Continuar a Pago',
        cancelButtonText:
          'Continuar Firmando otra empresa'
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'El proceso de firma no pudo ser realizado exitosamente por favor inténtelo de nuevo'
      });
    }

  }

}
