import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
// import { DBService } from '../../../services/db.services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  contactenos = 'https://www.ccb.org.co/Contactenos';
  message = 'menu seleccionado';
  mes = 'cerrar';
  menu: boolean;
  cuf: string;
  expandirpant: any;
  @Output() openMenu = new EventEmitter<string>();
  @Output() closeMenu = new EventEmitter<string>();

  constructor( private router: Router,
              //  private dbService: DBService
               ) { }

  // ngOnInit() {
  //   this.dbService.cuf.subscribe( cuf => this.cuf = cuf );
  // }

  onMenu() {
    this.openMenu.emit(this.message);
    this.menu = !this.menu;
  }

  offMenu() {
    this.closeMenu.emit(this.mes);
    this.menu = !this.menu;
  }

  expandir() {
    const el = document.documentElement;
    const rfs =  el.requestFullscreen;
    if (typeof rfs !== 'undefined' && rfs) {
        rfs.call(el);
    }
  }

  logout(): void {
    Swal.fire({
      title: 'Confirmar Salida',
      html: '¿Seguro que desea salir? <br>'  +
      'Tenga en cuenta que puede perder los datos no guardados.',
      icon: 'warning',
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.router.navigate(['/formulario']);
      }
    });
  }

}
