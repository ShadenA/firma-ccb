import { Component, OnInit, Input, OnChanges } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnChanges {

  contactenos = 'https://www.ccb.org.co/Contactenos';
  info: boolean;
  menu: string;
  @Input() abrirmenu: string;
  @Input() cerrarmenu: string;

  constructor() {

    this.info = true;
  }
  ngOnInit() {}

  ngOnChanges() {

    if (this.abrirmenu === 'menu seleccionado') {
      this.info = false;
      $(document).ready(() => {
          $('.firma').css('width', '200px');
      });
    }
    if (this.cerrarmenu === 'cerrar') {
      this.info = true;
      $(document).ready(() => {
        $('.firma').css('width', '120px');
      });
    }
  }

}
