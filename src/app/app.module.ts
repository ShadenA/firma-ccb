// modules
import { NgModule } from '@angular/core';
import { MatTreeModule } from '@angular/material/tree';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { AppRoutingModule } from './app-routing.module';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// components
import { AppComponent } from './app.component';
import { FirmaComponent } from './components/pages/firma/firma.component';
import { HeaderComponent } from './components/share/header/header.component';
import { NavbarComponent } from './components/share/navbar/navbar.component';
import { TableComponent } from './components/pages/table/table.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FirmaComponent,
    NavbarComponent,
    TableComponent
  ],
  imports: [
    MatTreeModule,
    MatCardModule,
    BrowserModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatDialogModule,
    MatButtonModule,
    AppRoutingModule,
    MatStepperModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
